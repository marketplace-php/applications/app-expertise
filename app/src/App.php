<?php

namespace Marketplace\App\Expertise;

use Marketplace\App\Kernel;

/**
 * Class App
 * @package Marketplace\App\Expertise
 */
class App {

	/**
	 * @return string
	 */
	public static function getType() {
		$get = Kernel\Request::getParam( 'get' );
		$out = Kernel\Parser::normalizeData( $get );

		return $out;
	}

	/**
	 * @throws \Exception
	 */
	public static function getPage() {
		if ( ! Config\General::getService( 'file.upload' )['enable'] ) {
			exit( 0 );
		}

		Kernel\View::get( 'form.file.upload', 'page' );
	}

	/**
	 * @throws \Exception
	 */
	public static function runApp() {
		System::createToken();
		self::getPage();
	}
}