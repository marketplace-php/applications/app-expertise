<?php

namespace Marketplace\App\Expertise\Config;

use Marketplace\App\Kernel;

/**
 * Class Ticket
 * @package Marketplace\App\Expertise\Config
 */
class General {

	/**
	 * @return mixed
	 */
	public static function getConfig() {
		$out = Kernel\Config::getFile( 'general' );

		return $out['general'];
	}

	/**
	 * @param $param
	 *
	 * @return mixed
	 */
	public static function getSystem( $param ) {
		$get = self::getConfig();

		$out = $get['system'][ $param ] ?? '' ?: [];

		return $out;
	}

	/**
	 * @param $service
	 *
	 * @return mixed
	 */
	public static function getService( $service ) {
		$get = self::getConfig();

		$out = $get['service'][ $service ] ?? '' ?: [];

		return $out;
	}
}