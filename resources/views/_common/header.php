<?php use Marketplace\App\Kernel\Route;
use Marketplace\App\Expertise\Config; ?>

<!DOCTYPE html>
<html dir="ltr" lang="ru">
<head prefix="og: http://ogp.me/ns#">
	<meta charset="utf-8" />
	<meta name="viewport" content="width=device-width, initial-scale=1" />
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="keywords" content="" />
	<meta name="description" content="" />
	<meta name="copyright" content="METADATA / FOUNDATION" />
	<meta name="robots" content="noindex, nofollow" />
	<title><?php echo Config\General::getSystem( 'name' ) ?></title>

	<!-- open graph -->
	<meta property="og:type" content="website" />
	<meta property="og:site_name" content="<?php echo Config\General::getSystem( 'name' ) ?>" />
	<meta property="og:title" content="<?php echo Config\General::getSystem( 'name' ) ?>" />
	<meta property="og:description" content="" />
	<meta property="og:image" content="" />
	<meta property="og:url" content="<?php echo Route::HTTP_HOST() ?>" />
	<!-- / open graph -->

	<!-- twitter -->
	<meta name="twitter:card" content="summary" />
	<meta name="twitter:title" content="<?php echo Config\General::getSystem( 'name' ) ?>" />
	<meta name="twitter:description" content="" />
	<meta name="twitter:image" content="" />
	<meta name="twitter:site" content="" />
	<meta name="twitter:creator" content="" />
	<!-- / twitter -->

	<!-- styles -->
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,400i,700,700i&amp;subset=cyrillic" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:400,400i,700,700i&amp;subset=cyrillic" />
	<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Fira+Mono:400,700&amp;subset=cyrillic" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/@fortawesome/fontawesome-free/css/all.min.css" />
	<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bulma/css/bulma.min.css" />
	<link rel="stylesheet" href="/resources/assets/styles/theme.css" />
	<!-- / styles -->

	<!-- favicon -->
	<link rel="icon" type="image/x-icon" href="/favicon.ico" />
	<!-- / favicon -->
</head>
<body itemscope itemtype="http://schema.org/WebPage">
